## newdata.txt to array using async/await

### How to run:
- Use npm install on local folder. This will install/update the dependencies.  
- Run the server by writing "node Server.js". 
- Open browser type: http://localhost:8080/  You will see the array.

### Used: 

- Express
- fs module specifically fs.promises
- cors

##### Side Note: It was a junior's doubt.

##### Checking if mirroring works in gitlab and google cloud repositories

### Contributor: Rishabh Gupta
