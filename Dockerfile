FROM node:13-slim

LABEL MAINTAINER=gupta.rishabh606@gmail.com

# Copy source code to /src in container
WORKDIR /src

ADD . /src

# Run this command (starts the app) when the container starts
CMD node Server.js
